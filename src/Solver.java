
public class Solver {

	private final MinPQ<SearchNode> mPQ = new MinPQ<SearchNode>();
	private final Stack<Board> sb = new Stack<Board>();
	private final int moves;
	private final boolean sovleable;

	private class SearchNode implements Comparable<SearchNode> {
		private Board board;
		private final SearchNode previous;
		private final int moves;

		public SearchNode(Board b, SearchNode p, int count) {
			board = b;
			previous = p;
			moves = count;
		}

		public int compareTo(SearchNode that) {
			return (this.board.manhattan() + this.moves)
					- (that.board.manhattan() + that.moves);
		}
	}

	public Solver(Board initial) {
		SearchNode searchNode = new SearchNode(initial, null, 0);
		mPQ.insert(searchNode);

		//MinPQ<SearchNode> tempPQ = new MinPQ<SearchNode>();
		SearchNode tempNode = new SearchNode(initial.twin(), null, 0);
		//tempPQ.insert(tempNode);

		while (!mPQ.min().board.isGoal() && !tempNode.board.isGoal()) {
			searchNode = mPQ.delMin();
			for (Board neighbor : searchNode.board.neighbors()) {
				if (doesNotHave(searchNode,neighbor)) {
					SearchNode temp = new SearchNode(neighbor, searchNode,
							searchNode.moves + 1);
					mPQ.insert(temp);
				}
			}
			tempNode = new SearchNode(mPQ.min().board.twin(),null,0);
//			tempNode = tempPQ.delMin();
//			for (Board neighbor : tempNode.board.neighbors()) {
//				if (tempNode.previous == null
//						|| !tempNode.previous.board.equals(neighbor)) {
//					SearchNode temp = new SearchNode(neighbor, tempNode,
//							tempNode.moves + 1);
//					tempPQ.insert(temp);
//				}
//			}
		}
		sovleable = mPQ.min().board.isGoal();
		if (sovleable) {
			moves = mPQ.min().moves;
		} else {
			moves = -1;
		}
		SearchNode sn = mPQ.min();
		while (sn != null) {
			sb.push(sn.board);
			sn = sn.previous;
		}

	}

	public boolean isSolvable(){
		return sovleable;
	}
	public int moves() {
		return moves;
	}
	
	private boolean doesNotHave(SearchNode sn,Board b){
		while(sn.previous!=null){
			if(b.equals(sn.previous.board)){
				return false;
			}
			sn=sn.previous;
		}
		return true;
	}
	
	public Iterable<Board> solution(){
		if(!sovleable){
			return null;
		}
		return sb;
	}
	
	public static void main(String[] args){
		In in = new In(args[0]);
		int N=in.readInt();
		int[][] blocks=new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				blocks[i][j] = in.readInt();
			}
		}
		Board initial = new Board(blocks);
		
		Solver solver = new Solver(initial);
		if(!solver.isSolvable()){
			StdOut.println("no solution possible");
		} else {
			StdOut.println("Minimum number of moves :"+solver.moves);
			for(Board board : solver.solution()){
				StdOut.println(board);
			}
		}
        // for each command-line argument
//      for (String filename : args) {
//
//            // read in the board specified in the filename
//            In in = new In(filename);
//            int N = in.readInt();
//            int[][] tiles = new int[N][N];
//            for (int i = 0; i < N; i++) {
//                for (int j = 0; j < N; j++) {
//                    tiles[i][j] = in.readInt();
//                }
//            }
//
//            // solve the slider puzzle
//            Board initial = new Board(tiles);
//            Solver solver = new Solver(initial);
//            System.out.println(filename + ": " + solver.moves());
//        }
//		
//	
	}

}
