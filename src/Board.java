public class Board {

	private final int[][] board;
	private final int row;
	private final int column;

	public Board(int[][] blocks) {
		int N = blocks.length;
		board = new int[N][N];
		int x = 0, y = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				board[i][j] = blocks[i][j];
				if (blocks[i][j] == 0) {
					x = i;
					y = j;
				}
			}
		}
		row = x;
		column = y;
	}

	public int dimension() {
		return board.length;
	}

	public int hamming() {
		int N = board.length;
		int sum = 0;
		int goal = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				goal++;
				if (board[i][j] != 0 && board[i][j] != goal && goal != N * N)
					sum++;
			}
		}
		if (board[N - 1][N - 1] != 0)
			sum++;
		return sum;
	}

	public int manhattan() {
		int N = board.length;
		int sum = 0;
		int goal = 0;
		int i_goal, j_goal;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				goal++;
				if (board[i][j] != 0 && board[i][j] != goal) {
					i_goal = board[i][j] / N;
					j_goal = board[i][j] % N;
					if(j_goal == 0){
						sum += Math.abs(i_goal - 1 - i) + Math.abs(N - 1 - j);
					} else{
						sum += Math.abs(i_goal - i) + Math.abs(j_goal - j - 1);
					}
				}
			}
		}
		return sum;
	}

	public boolean isGoal() {
		int N = board.length;
		int goal = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				goal++;
				if (board[i][j] != goal) {
					if (i == N - 1 && j == N - 1)
						return board[i][j] == 0;
					return false;
				}
			}
		}
		return true;
	}

	public Board twin() {
		int N = this.dimension();
		int[][] tempBoard = new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				tempBoard[i][j] = board[i][j];
			}
		}
		if (tempBoard[0][0] != 0 && tempBoard[0][1] != 0) {
			int temp = tempBoard[0][1];
			tempBoard[0][1] = tempBoard[0][0];
			tempBoard[0][0] = temp;
		} else {
			int temp1 = tempBoard[1][0];
			tempBoard[1][0] = tempBoard[1][1];
			tempBoard[1][1] = temp1;
		}
		Board twinBoard = new Board(tempBoard);
		return twinBoard;
	}

	public boolean equals(Object y) {
		if (y == this)
			return true;
		if (y == null)
			return false;
		if (y.getClass() != this.getClass())
			return false;
		Board that = (Board) y;
		if (this.dimension() != that.dimension())
			return false;
		for (int i = 0; i < this.dimension(); i++) {
			for (int j = 0; j < that.dimension(); j++) {
				if (that.board[i][j] != this.board[i][j])
					return false;
			}
		}
		return true;
	}

	public Iterable<Board> neighbors() {
		Queue<Board> qb = new Queue<Board>();
		int i = row, j = column;
		if (isValid(i, j - 1))
			qb.enqueue(swap(i, j - 1, i, j));
		if (isValid(i, j + 1))
			qb.enqueue(swap(i, j + 1, i, j));
		if (isValid(i - 1, j))
			qb.enqueue(swap(i - 1, j, i, j));
		if (isValid(i + 1, j))
			qb.enqueue(swap(i + 1, j, i, j));
		return qb;
	}

	private boolean isValid(int i, int j) {
		int N = this.dimension();
		if (i >= N || i < 0 || j >= N || j < 0)
			return false;
		return true;
	}

	private Board swap(int ic, int jc, int ip, int jp) {
		int N = this.dimension();
		int[][] tempBoard = new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				tempBoard[i][j] = board[i][j];
			}
		}
		int temp = tempBoard[ic][jc];
		tempBoard[ic][jc] = tempBoard[ip][jp];
		tempBoard[ip][jp] = temp;
		Board swapBoard = new Board(tempBoard);
		return swapBoard;
	}

	public String toString() {
		int N = board.length;
		StringBuilder s = new StringBuilder();
		s.append(N + "\n");
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				s.append(String.format("%2d ", board[i][j]));
			}
			s.append("\n");
		}
		return s.toString();
	}

	public static void main(String[] args) {
//		In in = new In("puzzle-unsolvable3x3.txt");
		In in = new In("puzzle04.txt");
		int N = in.readInt();
		int[][] blocks = new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				blocks[i][j] = in.readInt();
			}
		}
		Board b = new Board(blocks);
		
		StdOut.println(b.manhattan());
		StdOut.println(b.hamming());
		for (Board neighbor : b.neighbors()) {
			StdOut.println(neighbor.toString());
		}
		StdOut.println(b.toString());
		StdOut.println(b.isGoal());

	}

}
